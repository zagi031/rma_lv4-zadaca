package com.example.birdobserver.utilities

import com.example.birdobserver.R
import com.example.birdobserver.models.TextViewAppearance

object TextViewAppearanceHelper {
    const val BLACKBIRD_KEY = "black_bird"
    const val WHITEBIRD_KEY = "white_bird"
    const val GRAYBIRD_KEY = "gray_bird"
    const val BROWNBIRD_KEY = "brown_bird"
    const val NONE_KEY = "none"

    fun getTextViewAppearance(birdColor: String): TextViewAppearance {
        return when(birdColor){
            BLACKBIRD_KEY -> TextViewAppearance(R.drawable.bg_blackbird, R.color.white)
            WHITEBIRD_KEY -> TextViewAppearance(R.drawable.bg_whitebird, R.color.black)
            GRAYBIRD_KEY -> TextViewAppearance(R.drawable.bg_graybird, R.color.white)
            BROWNBIRD_KEY -> TextViewAppearance(R.drawable.bg_brownbird, R.color.white)
            else -> TextViewAppearance(R.color.transparent, R.color.black)
        }
    }
}
