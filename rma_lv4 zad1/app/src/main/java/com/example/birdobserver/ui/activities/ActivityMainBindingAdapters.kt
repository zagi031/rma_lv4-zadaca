package com.example.birdobserver.ui.activities

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("setBackgroundColor")
fun TextView.setBackgroundColor(resourceID:Int) {
    this.setBackgroundResource(resourceID)
}

@BindingAdapter("setTextColor")
fun TextView.setTextColor(resourceID:Int) {
    this.setTextColor(this.resources.getColor(resourceID))
}