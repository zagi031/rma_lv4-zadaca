package com.example.birdobserver

import android.app.Application

class BirdObserver : Application() {

    companion object {
        lateinit var context: BirdObserver
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }
}