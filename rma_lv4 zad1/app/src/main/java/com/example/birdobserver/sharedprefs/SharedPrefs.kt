package com.example.birdobserver.sharedprefs

import com.example.birdobserver.models.TextViewAppearance

interface SharedPrefs {
    fun saveBirdCount(birdCount: Int)
    fun getBirdCount(): Int
    fun saveBirdCounterAppearance(appearance: TextViewAppearance)
    fun getBirdCounterAppearance(): TextViewAppearance
}