package com.example.birdobserver.sharedprefs

import android.content.Context
import com.example.birdobserver.BirdObserver
import com.example.birdobserver.models.TextViewAppearance
import com.example.birdobserver.utilities.TextViewAppearanceHelper
import com.google.gson.Gson

object SharedPrefsManager : SharedPrefs {

    private const val SHAREDPREFS_KEY = "shared_preferences"
    private const val BIRDCOUNT_KEY = "bird_counter"
    private const val TVBIRDCOUNTERAPPEARANCE_KEY = "tv_birdCounterAppearance_key"


    private val sharedPrefs =
        BirdObserver.context.getSharedPreferences(SHAREDPREFS_KEY, Context.MODE_PRIVATE)

    override fun saveBirdCount(birdCount: Int) =
        sharedPrefs.edit().putInt(BIRDCOUNT_KEY, birdCount).apply()

    override fun getBirdCount() = sharedPrefs.getInt(BIRDCOUNT_KEY, 0)

    override fun saveBirdCounterAppearance(appearance: TextViewAppearance) =
        sharedPrefs.edit().putString(
            TVBIRDCOUNTERAPPEARANCE_KEY, Gson().toJson(appearance)
        ).apply()

    override fun getBirdCounterAppearance(): TextViewAppearance {
        val json =
            sharedPrefs.getString(TVBIRDCOUNTERAPPEARANCE_KEY, TextViewAppearanceHelper.NONE_KEY)
                .toString()
        return if (json == TextViewAppearanceHelper.NONE_KEY) {
            TextViewAppearanceHelper.getTextViewAppearance(TextViewAppearanceHelper.NONE_KEY)
        } else Gson().fromJson(json, TextViewAppearance::class.java)
    }
}