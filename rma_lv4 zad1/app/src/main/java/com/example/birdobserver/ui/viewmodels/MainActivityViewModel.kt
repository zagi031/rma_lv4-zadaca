package com.example.birdobserver.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.birdobserver.models.TextViewAppearance
import com.example.birdobserver.sharedprefs.SharedPrefs
import com.example.birdobserver.sharedprefs.SharedPrefsManager
import com.example.birdobserver.utilities.TextViewAppearanceHelper
import com.example.birdobserver.utilities.TextViewAppearanceHelper.getTextViewAppearance

class MainActivityViewModel : ViewModel() {
    private val sharedPrefs: SharedPrefs = SharedPrefsManager
    private val _birdCounter = MutableLiveData<Int>(sharedPrefs.getBirdCount())
    val birdCounter: LiveData<Int>
        get() = _birdCounter
    private val _textViewAppearance =
        MutableLiveData<TextViewAppearance>(sharedPrefs.getBirdCounterAppearance())
    val textViewAppearance: LiveData<TextViewAppearance>
        get() = _textViewAppearance

    fun seeBird(birdColor: String) {
        incrementBirdCounter()
        setTextViewAppearance(birdColor)
    }

    private fun incrementBirdCounter() = _birdCounter.value?.let {
        _birdCounter.postValue(it + 1)
    }

    private fun setTextViewAppearance(birdColor: String) =
        _textViewAppearance.postValue(getTextViewAppearance(birdColor))

    fun resetCounter() {
        _birdCounter.postValue(0)
        _textViewAppearance.postValue(getTextViewAppearance(TextViewAppearanceHelper.NONE_KEY))
    }

    fun saveToSharedPrefs() {
        sharedPrefs.saveBirdCount(_birdCounter.value!!)
        sharedPrefs.saveBirdCounterAppearance(_textViewAppearance.value!!)
    }
}