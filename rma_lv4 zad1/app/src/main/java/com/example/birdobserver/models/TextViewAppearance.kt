package com.example.birdobserver.models

data class TextViewAppearance(val backgroundResID: Int, val textColorResID: Int)
